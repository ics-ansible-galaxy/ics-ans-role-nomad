import os
import re
import testinfra.utils.ansible_runner


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_nomad_instance(host):
    cmd1 = host.run('hostname -I | awk "{print $1}"')
    ip = re.sub(r'[\n\s]*', '', cmd1.stdout)
    cmd = host.run('curl -I http://' + ip + ':4646/ui/')
    assert '200' in cmd.stdout
